# Webflux Demo

Requirements to be able to run this project
- JDK 16
- Mongo DB


If you haven't yet installed gradle, don't worry. Intellij IDEA itself will download and install for you.



First, create some players using **/api/player/v1** POST method.
You can use swagger generated UI for this:                                                                                                    
 - _http://localhost:8081/webjars/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/player-controller/add_



Then use  **google chrome's**  latest version to request  **/api/player/v1**'s GET method. 
Postman or some brausers don't support _text/event-stream_ , that's why we should use Google chrome.  
