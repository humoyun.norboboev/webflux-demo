package uz.javaholics.demo.service;

import uz.javaholics.demo.domain.PlayerEntity;
import uz.javaholics.demo.model.PlayerAddDto;
import uz.javaholics.demo.model.PlayerDto;
import uz.javaholics.demo.repository.PlayerRepository;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

import static org.springframework.util.Assert.hasText;
import static org.springframework.util.Assert.notNull;

@Slf4j
@Service
public class PlayerService {

    private final PlayerRepository playerRepository;

    private final ObjectMapper objectMapper;

    public PlayerService(PlayerRepository playerRepository, ObjectMapper objectMapper) {
        this.playerRepository = playerRepository;
        this.objectMapper = objectMapper;
    }

    public Mono<Void> add(PlayerAddDto addDto) {

        hasText(addDto.firstName(), "[firstName] must have text! ");
        hasText(addDto.lastName(), "[firstName] must have text! ");
        notNull(addDto.number(), "[number] cannot be null! ");

        return Mono.just(addDto)
                .map(dto -> {
                    PlayerEntity playerEntity = new PlayerEntity();

                    try {
                        return objectMapper.updateValue(playerEntity, addDto);
                    } catch (JsonMappingException e) {
                        throw new RuntimeException("Body could not be parsed to PlayerEntity ! ");
                    }
                })
                .flatMap(playerRepository::save)
                .then();
    }

    public Flux<PlayerDto> getAll() {

//        pipeline is set of pure function. It is a design.
//        pure function is the function which doesn't change input param

//        there we can see threads' change for not to block one
        log.info("Thread Id Before finding sending request to DB: " + Thread.currentThread().getId());

        return playerRepository
                .findAll()
                .delayElements(Duration.ofSeconds(1))
//                delay elements streams player info 1 second range
                .flatMap(playerEntity -> {
                    log.info("Current Thread Id ==> " + Thread.currentThread().getId());
                    log.info("Current Thread Name ==> " + Thread.currentThread().getName());
                    return Mono.just(objectMapper.convertValue(playerEntity, PlayerDto.class));
                });
    }
}
