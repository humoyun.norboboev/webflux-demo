package uz.javaholics.demo.model;


public record PlayerAddDto(String firstName, String lastName, String clubName, Integer number) {
}
