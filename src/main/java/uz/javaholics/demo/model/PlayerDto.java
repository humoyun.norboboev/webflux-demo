package uz.javaholics.demo.model;

import java.util.UUID;

public record PlayerDto(UUID id, String firstName, String lastName, String clubName, Integer number) {
}
