package uz.javaholics.demo.repository;

import uz.javaholics.demo.domain.PlayerEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import java.util.UUID;

public interface PlayerRepository extends ReactiveMongoRepository<PlayerEntity, UUID> {
}
