package uz.javaholics.demo.domain;

import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;

@Data // @Getter, @Setter, @AllArgsConstructor, @NoArgsConstructor, @ToString, @EqualsAndHashCode
@Document(collection = "player")
@FieldDefaults(level = PRIVATE)
public class PlayerEntity {

    @Id
    UUID id = UUID.randomUUID();

    String firstName;

    String lastName;

    String clubName;

    Integer number;

}
