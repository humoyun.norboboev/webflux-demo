package uz.javaholics.demo.controller;

import uz.javaholics.demo.model.PlayerAddDto;
import uz.javaholics.demo.model.PlayerDto;
import uz.javaholics.demo.service.PlayerService;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@RestController
@RequestMapping("/api/player/v1")
public class PlayerController {

    private final PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @PostMapping
    Mono<Void> add(@RequestBody final PlayerAddDto playerAddDto) {
        return playerService.add(playerAddDto);
    }

    @GetMapping(produces = TEXT_EVENT_STREAM_VALUE)  // this api produces text/event-stream, it means response will be streamed.
    Flux<PlayerDto> get() {
        return playerService.getAll();
    }

}
